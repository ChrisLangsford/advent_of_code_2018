package za.co.langsford.advent.of.code8.day;

import org.junit.Test;
import solutions.D3Claim;
import solutions.DayThree;

import static org.junit.Assert.assertEquals;

public class DayThreeTest {

    @Test
    public void claimCreationTest() {
        D3Claim d3Claim = new D3Claim("1 1 3 4 4");

        assertEquals(new Integer(1), d3Claim.getId());
        assertEquals(new Integer(1), d3Claim.getSkipX());
        assertEquals(new Integer(3), d3Claim.getSkipY());
        assertEquals(new Integer(4), d3Claim.getWidth());
        assertEquals(new Integer(4), d3Claim.getHeight());
    }

    @Test
    public void part1Test() {
        DayThree dayThree = new DayThree("file/input3.1.txt");
        assertEquals("Day 3 Part 1 Solution: 4", dayThree.runPartOne());
    }

    @Test
    public void part2Test() {
        DayThree dayThree = new DayThree("file/input3.1.txt");
        assertEquals("Day 3 Part 2 Solution: 3", dayThree.runPartTwo());
    }
}
