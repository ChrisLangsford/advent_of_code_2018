package za.co.langsford.advent.of.code8.day;

import org.junit.Test;
import solutions.D4Shift;
import solutions.DayFour;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DayFourTest {

    @Test
    public void shiftSleepDurationCalculation() {
        D4Shift d4Shift = new D4Shift();
        d4Shift.setGuardId(1);
        List<Integer> starts = new ArrayList<>();
        starts.add(5);
        d4Shift.setSleepMinute(starts);
        List<Integer> ends = new ArrayList<>();
        ends.add(25);
        d4Shift.setWakeMinute(ends);

        assertEquals(new Integer(20), d4Shift.getSleepDuration());
    }

    @Test
    public void part1Test() {
        DayFour dayFour = new DayFour("file/input4.1.txt");
        assertEquals("Day 4 Part 1 Solution: 240", dayFour.runPartOne());
    }

    @Test
    public void part2Test() {
        DayFour dayFour = new DayFour("file/input4.1.txt");
        assertEquals("Day 4 Part 2 Solution: 4455", dayFour.runPartTwo());
    }
}
