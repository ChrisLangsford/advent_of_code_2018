package za.co.langsford.advent.of.code8.day;

import org.junit.Before;
import org.junit.Test;
import solutions.DaySix;

import static org.junit.Assert.assertEquals;

public class DaySixTest {
    DaySix daySix;

    @Before
    public void setUp() throws Exception {
        daySix = new DaySix("file/input6.1.txt");
    }

    @Test
    public void part1Test() {
        assertEquals("Day 6 Part 1 Solution: 17", daySix.runPartOne());
    }

    @Test
    public void part2Test() {
        assertEquals("Day 6 Part 2 Solution: 16", daySix.runPartTwo());
    }
}
