package za.co.langsford.advent.of.code8.day;

import org.junit.Before;
import org.junit.Test;
import solutions.DayTwo;

import static org.junit.Assert.assertEquals;

public class DayTwoTest {

    private DayTwo dayTwo;

    @Before
    public void setUp() throws Exception {
        dayTwo = new DayTwo("file/input2.1.txt");
    }

    @Test
    public void part1Test() {
        assertEquals("Day 2 Part 1 Solution: 12", dayTwo.runPartOne());
    }

    @Test
    public void letterCountMapTest() {
        assertEquals(2, dayTwo.countLettersInString("babac").get('b').intValue());
        assertEquals(2, dayTwo.countLettersInString("babac").get('a').intValue());
        assertEquals(1, dayTwo.countLettersInString("babac").get('c').intValue());

    }

    @Test
    public void TestMapFilter() {
        assertEquals(2, dayTwo.filterMapByOccurrences(dayTwo.countLettersInString("babac"),2).size());
        assertEquals(0, dayTwo.filterMapByOccurrences(dayTwo.countLettersInString("babac"),3).size());
    }

    @Test
    public void Part2Test() {
        assertEquals("Day 2 Part 2 Solution: fgij", dayTwo.runPartTwo());
    }
}
