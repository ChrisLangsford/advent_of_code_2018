package za.co.langsford.advent.of.code8.day;

import org.junit.Test;
import solutions.DayOne;

import static org.junit.Assert.assertEquals;

public class DayOneTest {


    @Test
    public void PartTwoTest1() {
        DayOne dayOne = new DayOne("file/input1.1.txt");
        assertEquals("Day 1 Part 2 Solution: 0", dayOne.runPartTwo());
    }

    @Test
    public void PartTwoTest2() {
        DayOne dayOne = new DayOne("file/input1.2.txt");
        assertEquals("Day 1 Part 2 Solution: 10", dayOne.runPartTwo());
    }

    @Test
    public void PartTwoTest3() {
        DayOne dayOne = new DayOne("file/input1.3.txt");
        assertEquals("Day 1 Part 2 Solution: 5", dayOne.runPartTwo());
    }

    @Test
    public void PartTwoTest4() {
        DayOne dayOne = new DayOne("file/input1.4.txt");
        assertEquals("Day 1 Part 2 Solution: 14", dayOne.runPartTwo());
    }
}
