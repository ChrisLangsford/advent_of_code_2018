package za.co.langsford.advent.of.code8.day;

import org.junit.Test;
import solutions.DayFive;

import static org.junit.Assert.assertEquals;

public class DayFiveTest {

    @Test
    public void part1Test() {
        DayFive dayFive = new DayFive("file/input5.1.txt");

        assertEquals("Day 5 Part 1 Solution: 10", dayFive.runPartOne());
    }

    @Test
    public void part2Test() {
        DayFive dayFive = new DayFive("file/input5.1.txt");

        assertEquals("Day 5 Part 2 Solution: 4", dayFive.runPartTwo());
    }
}
