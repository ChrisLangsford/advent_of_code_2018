package za.co.langsford.advent.of.code8.day;

import org.junit.Before;
import org.junit.Test;
import solutions.DaySeven;

import static org.junit.Assert.assertEquals;

public class DaySevenTest {

    private DaySeven daySeven;

    @Before
    public void setUp() throws Exception {
        daySeven = new DaySeven("file/input7.1.txt");
    }

    @Test
    public void part1Test() {
        assertEquals("Day 7 Part 1 Solution: CABDFE", daySeven.runPartOne());
    }

    @Test
    public void part2Test() {
        assertEquals("Day 7 Part 2 Solution: 0", daySeven.runPartTwo());
    }
}
