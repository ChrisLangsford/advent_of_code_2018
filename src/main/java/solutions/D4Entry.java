package solutions;

public class D4Entry {
    private Integer hour;
    private Integer min;
    private String entry;

    public D4Entry() {
    }

    public D4Entry(String line) {
        String[] parts = line.split(" ");
        this.hour = Integer.parseInt(parts[0]);
        this.min = Integer.parseInt(parts[1]);
        this.entry = parts[2];

    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }
}
