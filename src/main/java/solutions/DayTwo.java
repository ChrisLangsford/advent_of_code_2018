package solutions;

import sun.rmi.runtime.Log;
import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.*;

public class DayTwo extends AocSolution {
    public DayTwo(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        Integer twos = 0;
        Integer threes = 0;

        for (String string : processInputFile()) {
            if (stringHasXLetters(string, 3)) {
                twos++;
            }
            if (stringHasXLetters(string, 2)) {
                threes++;
            }
        }

        Integer result = twos * threes;
        return String.format("Day 2 Part 1 Solution: %d", result);
    }

    @Override
    public String runPartTwo() {
        Set<String> matchingStrings= new HashSet<>();

        for (String one: processInputFile()) {
            for (String two: processInputFile()) {
                if (stringsHaveHammingDistanceOfOne(one, two)) {
                    matchingStrings.add(one);
                    matchingStrings.add(two);
                }
            }
        }
        matchingStrings.forEach(string-> LOG.info(string.toString()));
        return String.format("Day 2 Part 2 Solution: %d",0);
    }

    private Boolean stringsHaveHammingDistanceOfOne(String one, String two) {
        int counter = 0;
        for (int i = 0; i < one.length(); i++) {
            if (one.charAt(i) != two.charAt(i)) {
                counter++;
            }
        }
        return counter == 1;
    }


    private Boolean stringHasXLetters(String s, Integer i) {
        return filterMapByOccurrences(countLettersInString(s), i).size() > 0;
    }


    public Map<Character, Integer> filterMapByOccurrences(Map<Character, Integer> map, Integer occurrences) {
        Map<Character, Integer> newMap = new HashMap<>();
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue().equals(occurrences)) {
                newMap.put(entry.getKey(), entry.getValue());
            }
        }
        return newMap;
    }

    public Map<Character, Integer> countLettersInString(String string) {
        Map<Character, Integer> letterCounts = new HashMap<>();
        for (char c : string.toCharArray()) {
            letterCounts.merge(c, 1, (a, b) -> a + b);
        }

        return letterCounts;
    }

    private List<String> processInputFile() {
        List<String> inputs = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                inputs.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            LOG.error("FIle not found");
        }
        return inputs;
    }
}
