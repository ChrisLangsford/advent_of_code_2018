package solutions;

import java.util.Comparator;

public class D6Cell {
    private Integer cellId;
    private Integer x;
    private Integer y;
    private Boolean isGivenCoord;
    private Integer nearestCoordId;

    public D6Cell(Integer x, Integer y, Boolean isGivenCoord) {
        this.x = x;
        this.y = y;
        this.isGivenCoord = isGivenCoord;
    }

    public D6Cell(Integer cellId, Integer x, Integer y, Boolean isGivenCoord) {
        this.cellId = cellId;
        this.x = x;
        this.y = y;
        this.isGivenCoord = isGivenCoord;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Boolean getGivenCoord() {
        return isGivenCoord;
    }

    public void setGivenCoord(Boolean givenCoord) {
        isGivenCoord = givenCoord;
    }

    public Integer getNearestCoordId() {
        return nearestCoordId;
    }

    public void setNearestCoordId(Integer nearestCoordId) {
        this.nearestCoordId = nearestCoordId;
    }
}

class SortByX implements Comparator<D6Cell> {
    public int compare(D6Cell a, D6Cell b) {
        return a.getX() - b.getX();
    }
}

class SortByY implements Comparator<D6Cell> {
    public int compare(D6Cell a, D6Cell b) {
        return a.getY() - b.getY();
    }
}
