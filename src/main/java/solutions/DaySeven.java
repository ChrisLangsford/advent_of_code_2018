package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class DaySeven extends AocSolution {

    Map<String, D7Step> stepMap = new HashMap<>();
    StringBuilder output = new StringBuilder();

    public DaySeven(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        getUniqueStepList();
        buildSteps();

        List<D7Step> startingSteps = new ArrayList<>();
        stepMap.forEach((stepId, step) -> {
            if (step.getPrecededBy() == null || step.getPrecededBy().isEmpty()) {
                step.setAvailable(true);
                startingSteps.add(step);
            }
        });
        Collections.sort(startingSteps);
        startingSteps.forEach(step -> processSteps(stepMap.get(step.getId())));
        LOG.info(output.toString());
        return String.format("Day 7 Part 1 Solution: %s", output);
    }

    private void processSteps(D7Step startingStep) {
        //check all prerequisite steps have been completed
        boolean allPrerequisitesCompleted = true;
        for (String stepId : startingStep.getPrecededBy()) {
            if (!stepMap.get(stepId).getCompleted()) {
                allPrerequisitesCompleted = false;
            }
        }
        if (allPrerequisitesCompleted) {
            //mark step as complete
            // & run through downstream steps if upstream steps are complete
            stepMap.get(startingStep.getId()).setCompleted(true);
            output.append(startingStep.getId());
            startingStep.getPrecedes().forEach(step -> processSteps(stepMap.get(step)));
        }
    }


    @Override
    public String runPartTwo() {
        return String.format("Day 7 Part 2 Solution: 0");
    }

    private void buildSteps() {
        processInputFile().forEach(line -> {
            stepMap.get(Character.toString(line.charAt(0))).getPrecedes().add(Character.toString(line.charAt(1)));
            stepMap.get(Character.toString(line.charAt(1))).getPrecededBy().add(Character.toString(line.charAt(0)));
        });
    }

    private void getUniqueStepList() {
        List<String> withDuplicates = new ArrayList<>();
        processInputFile().forEach(l -> {
            withDuplicates.add(Character.toString(l.charAt(0)));
            withDuplicates.add(Character.toString(l.charAt(1)));
        });
        List<String> noDuplicates = withDuplicates.stream()
                .distinct()
                .collect(Collectors.toList());
        noDuplicates.forEach(c -> stepMap.put(c, new D7Step(c)));

        //ensure all steps have their fields sorted correctly
        stepMap.forEach((stepId, step) -> {
            Collections.sort(step.getPrecedes());
            Collections.sort(step.getPrecededBy());
        });
    }

    private List<String> processInputFile() {
        List<String> inputs = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                inputs.add(scanner.nextLine());
            }

        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return inputs;
    }
}
