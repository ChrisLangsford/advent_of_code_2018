package solutions;

import java.util.ArrayList;
import java.util.List;

public class D7Step implements Comparable<D7Step> {
    private String id;
    private List<String> precedes = new ArrayList<>();
    private List<String> precededBy = new ArrayList<>();
    private Boolean available = false;
    private Boolean completed = false;

    public D7Step() {
    }

    public D7Step(String id) {
        this.id = id;
    }

    public D7Step(String id, List<String> precedes, List<String> precededBy) {
        this.id = id;
        this.precedes = precedes;
        this.precededBy = precededBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getPrecedes() {
        return precedes;
    }

    public void setPrecedes(List<String> precedes) {
        this.precedes = precedes;
    }

    public List<String> getPrecededBy() {
        return precededBy;
    }

    public void setPrecededBy(List<String> precededBy) {
        this.precededBy = precededBy;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public int compareTo(D7Step o) {
        if (getId() == null && o.getId() == null) {
            return 0;
        }
        return getId().compareTo(o.getId());
    }
}
