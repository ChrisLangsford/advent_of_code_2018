package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.*;

import static util.MapMethods.getMapMax;

public class DaySix extends AocSolution {

    private Integer minX;
    private Integer minY;
    private Integer maxX;
    private Integer maxY;

    private final Integer SAFE_DISTANCE = 32;

    public DaySix(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        List<D6Cell> inputs = processInputFile();
        calculateBoundaries(inputs);
        D6Cell[][] grid = new D6Cell[maxY + 1][maxX + 1];
        addCellsToGrid(inputs, grid);
        calculateClosestPoints(grid, inputs);
        Map<Integer, Integer> pointAreas = tallyDistanceCounts(grid, inputs);
        pointAreas = filterMap(pointAreas, inputs);
        getMapMax(pointAreas);
//        printGrid(grid);
        Integer result = pointAreas.get(getMapMax(pointAreas));
        return String.format("Day 6 Part 1 Solution: %d", result);
    }

    @Override
    public String runPartTwo() {
        List<D6Cell> inputs = processInputFile();
        calculateBoundaries(inputs);
        D6Cell[][] grid = new D6Cell[maxY + 1][maxX + 1];
        addCellsToGrid(inputs, grid);
        markSafeArea(grid, inputs);
        printGrid(grid);
        Integer result = calcSafeAreaSize(grid);
        return String.format("Day 6 Part 2 Solution: %d", result);
    }

    private void markSafeArea(D6Cell[][] grid, List<D6Cell> points) {
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                if (getDistanceFromAllPoints(grid[y][x], points) < SAFE_DISTANCE) {
                    grid[y][x].setNearestCoordId(1);
                } else {
                    grid[y][x].setNearestCoordId(0);
                }
            }
        }
    }

    private Integer getDistanceFromAllPoints(D6Cell start, List<D6Cell> points) {
        Map<Integer, Integer> pointDistanceMap = new HashMap<>();
        for (D6Cell point : points) {
            pointDistanceMap.put(point.getCellId(), 0);
        }
        for (D6Cell point : points) {
            pointDistanceMap.put(point.getCellId(), Math.abs(start.getX() - point.getX()) + Math.abs(start.getY() - point.getY()));
        }

        Integer totalDistance = 0;
        for (D6Cell point : points) {
            totalDistance += pointDistanceMap.get(point.getCellId());
        }
        return totalDistance;
    }

    private Integer calcSafeAreaSize(D6Cell[][] grid) {
        Integer area = 0;
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                if (grid[y][x].getNearestCoordId().equals(1)) {
                    area++;
                }
            }
        }
        return area;
    }

    private Map<Integer, Integer> filterMap(Map<Integer, Integer> pointAreas, List<D6Cell> points) {
        Map<Integer, Integer> filteredOutEdges = new HashMap<>();
        for (D6Cell point : points) {
            if (!point.getX().equals(minX) || !point.getX().equals(maxX) && !point.getY().equals(minY) || !point.getY().equals(maxY)) {
                filteredOutEdges.put(point.getCellId(), pointAreas.get(point.getCellId()));
            }
        }
        return filteredOutEdges;
    }

    public void printGrid(D6Cell[][] grid) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                if (grid[y][x].getNearestCoordId() == -1) {
                    stringBuilder.append("*");
                } else if (grid[y][x].getGivenCoord()) {
                    stringBuilder.append("&");
                } else {
                    stringBuilder.append(grid[y][x].getNearestCoordId());
                }
            }
            stringBuilder.append("\n");
        }
        LOG.info(stringBuilder.toString());
    }

    private Integer getMapMax(Map<Integer, Integer> map) {
        Map.Entry<Integer, Integer> maxEntry = null;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        if (maxEntry != null) {
            return maxEntry.getKey();
        }
        throw new RuntimeException("Max entry in map was null ");
    }

    private Map<Integer, Integer> tallyDistanceCounts(D6Cell[][] grid, List<D6Cell> points) {
        Map<Integer, Integer> pointAreaMap = new HashMap<>();
        for (D6Cell point : points) {
            pointAreaMap.put(point.getCellId(), 0);
        }
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                if (!grid[y][x].getNearestCoordId().equals(-1)) {
                    pointAreaMap.put(grid[y][x].getNearestCoordId(), pointAreaMap.get(grid[y][x].getNearestCoordId()) + 1);
                }
            }
        }
        return pointAreaMap;
    }

    private void calculateClosestPoints(D6Cell[][] grid, List<D6Cell> points) {
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                grid[y][x].setNearestCoordId(findNearestPoint(grid[y][x], points));
            }
        }
    }

    private Integer findNearestPoint(D6Cell start, List<D6Cell> points) {
        Map<Integer, Integer> distancesToPoints = new HashMap<>();
        for (D6Cell point : points) {
            distancesToPoints.put(point.getCellId(), Math.abs(start.getX() - point.getX()) + Math.abs(start.getY() - point.getY()));
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(distancesToPoints.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<Integer, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<Integer, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        if (list.get(0).getValue().equals(list.get(1).getValue())) {
            return -1;
        }
        return result.entrySet().iterator().next().getKey();
    }


    private void addCellsToGrid(List<D6Cell> inputs, D6Cell[][] grid) {
        inputs.forEach(cell -> grid[cell.getY()][cell.getX()] = cell);
        for (int y = 0; y <= maxY; y++) {
            for (int x = 0; x <= maxX; x++) {
                if (grid[y][x] == null) {
                    grid[y][x] = new D6Cell(x, y, false);
                }
            }
        }
    }

    private void calculateBoundaries(List<D6Cell> cells) {
        Collections.sort(cells, new SortByX());
        minX = cells.get(0).getX();
        maxX = cells.get(cells.size() - 1).getX();
        Collections.sort(cells, new SortByY());
        minY = cells.get(0).getY();
        maxY = cells.get(cells.size() - 1).getY();
    }

    private List<D6Cell> processInputFile() {
        List<D6Cell> inputs = new ArrayList<>();
        Integer counter = 0;
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                String[] coords = scanner.nextLine().split(",");
                inputs.add(new D6Cell(counter, Integer.parseInt(coords[0].trim()), Integer.parseInt(coords[1].trim()), true));
                counter++;
            }

        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return inputs;
    }
}
