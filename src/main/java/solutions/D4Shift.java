package solutions;

import java.util.ArrayList;
import java.util.List;

public class D4Shift {
    private Integer guardId;
    private List<Integer> sleepMinute = new ArrayList<>();
    private List<Integer> wakeMinute = new ArrayList<>();
    private Integer sleepDuration;

    public Integer getGuardId() {
        return guardId;
    }

    public void setGuardId(Integer guardId) {
        this.guardId = guardId;
    }

    public Integer getSleepDuration() {
        if (getSleepMinute().size() == getWakeMinute().size()) {
            sleepDuration = calculateSleepDuration();
            return sleepDuration;
        } else {
            throw new RuntimeException("The number of times fallen asleep does not equal the number of times awoken in this shift");
        }
    }

    private Integer calculateSleepDuration(){
        Integer totalSleepDuration = 0;
        for (int i = 0; i < sleepMinute.size(); i++) {
            totalSleepDuration += wakeMinute.get(i) - sleepMinute.get(i);
        }
        return totalSleepDuration;
    }

    public List<Integer> getSleepMinute() {
        return sleepMinute;
    }

    public void setSleepMinute(List<Integer> sleepMinute) {
        this.sleepMinute = sleepMinute;
    }

    public List<Integer> getWakeMinute() {
        return wakeMinute;
    }

    public void setWakeMinute(List<Integer> wakeMinute) {
        this.wakeMinute = wakeMinute;
    }
}
