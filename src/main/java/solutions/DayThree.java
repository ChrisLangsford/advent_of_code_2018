package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DayThree extends AocSolution {

    private Integer[][] grid = new Integer[1000][1000];

    public DayThree(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        initGrid();
        processInputFile().forEach(this::processClaim);
        return String.format("Day 3 Part 1 Solution: %d", countOverlappingSquares());
    }

    @Override
    public String runPartTwo() {
        initGrid();
        List<D3Claim> claims = processInputFile();
        claims.forEach(this::processClaim);
        String found = null;
        for (D3Claim claim : claims) {
            if (!claimOverlaps(claim)) {
                found = claim.getId().toString();
            }
        }
        return String.format("Day 3 Part 2 Solution: %s", found);
    }

    private Boolean claimOverlaps(D3Claim claim) {
        Boolean overlaps = false;
        for (int y = claim.getSkipY(); y < claim.getSkipY() + claim.getHeight(); y++) {
            for (int x = claim.getSkipX(); x < claim.getSkipX() + claim.getWidth(); x++) {
                if (grid[y][x] != 1) {
                    overlaps = true;
                    break;
                }
                if (overlaps) {
                    break;
                }
            }
        }
        return overlaps;
    }

    private void initGrid() {
        for (int y = 0; y < 1000; y++) {
            for (int x = 0; x < 1000; x++) {
                grid[y][x] = 0;
            }
        }
    }

    private Integer countOverlappingSquares() {
        Integer count = 0;
        for (int y = 0; y < 1000; y++) {
            for (int x = 0; x < 1000; x++) {
                if (grid[y][x] > 1) {
                    count++;
                }
            }
        }
        return count;
    }

    private void processClaim(D3Claim d3Claim) {
        for (int y = d3Claim.getSkipY(); y < d3Claim.getSkipY() + d3Claim.getHeight(); y++) {
            for (int x = d3Claim.getSkipX(); x < d3Claim.getSkipX() + d3Claim.getWidth(); x++) {
                grid[y][x]++;
            }
        }
    }

    private List<D3Claim> processInputFile() {
        List<D3Claim> inputs = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                inputs.add(new D3Claim(scanner.nextLine()));
            }
        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return inputs;
    }
}
