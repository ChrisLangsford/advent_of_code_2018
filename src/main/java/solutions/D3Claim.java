package solutions;

import java.util.Arrays;
import java.util.List;

public class D3Claim {

    private String claimString;
    private Integer id;
    private Integer skipX;
    private Integer skipY;
    private Integer width;
    private Integer height;

    public D3Claim() {
    }

    public D3Claim(String claimString) {
        this.claimString = claimString;
        convertStringToAttributes();
    }

    private void convertStringToAttributes() {
        List<String> values = Arrays.asList(getClaimString().split(" "));
        setId(Integer.parseInt(values.get(0)));
        setSkipX(Integer.parseInt(values.get(1)));
        setSkipY(Integer.parseInt(values.get(2)));
        setWidth(Integer.parseInt(values.get(3)));
        setHeight(Integer.parseInt(values.get(4)));
    }

    public String getClaimString() {
        return claimString;
    }

    public void setClaimString(String claimString) {
        this.claimString = claimString;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSkipX() {
        return skipX;
    }

    public void setSkipX(Integer skipX) {
        this.skipX = skipX;
    }

    public Integer getSkipY() {
        return skipY;
    }

    public void setSkipY(Integer skipY) {
        this.skipY = skipY;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
