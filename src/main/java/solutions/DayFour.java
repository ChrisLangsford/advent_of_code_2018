package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.*;

import static util.MapMethods.getMapMax;

public class DayFour extends AocSolution {

    private final String ASLEEP = "asleep";
    private final String WAKES = "wakes";

    private List<D4Shift> shifts = new ArrayList<>();
    private Set<Integer> guardIds = new HashSet<>();
    private Map<Integer, Integer> guardSleepTallies = new HashMap<>();
    private Map<Integer, Integer> guardMaxMinuteMap = new HashMap<>();
    private Integer maxTimesAsleepPerMinute = 0;
    private Integer guard;

    public DayFour(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        convertEntriesIntoShifts();
        initGuardMaxMinuteMap();
        sumMinutesSleptByEachGuard();
        Integer guardId = getMapMax(guardSleepTallies);
        getMostPopularMinutePerGuard(guardId);
        Integer popularMinute = guardMaxMinuteMap.get(guardId);
        LOG.info(String.format("Guard: %d, Minute:%d", guardId, guardMaxMinuteMap.get(guardId)));
        return String.format("Day 4 Part 1 Solution: %d", guardId * popularMinute);
    }

    @Override
    public String runPartTwo() {
        convertEntriesIntoShifts();
        initGuardMaxMinuteMap();
        guardIds.forEach(this::getMostPopularMinutePerGuard);
        Integer guardId = guard;
        Integer popularMinute = guardMaxMinuteMap.get(guardId);

        LOG.info(String.format("Guard: %d, Minute:%d", guardId, popularMinute));
        return String.format("Day 4 Part 2 Solution: %d", guardId * popularMinute);
    }

    private void initGuardMaxMinuteMap() {
        for (Integer guardId : guardIds) {
            guardMaxMinuteMap.put(guardId, 0);
        }
    }

    private void getMostPopularMinutePerGuard(Integer guardId) {
        Map<Integer, Integer> minuteMap = new HashMap<>();
        List<D4Shift> filteredShifts = filterShiftList(shifts, guardId);
        populateMinuteMap(filteredShifts, minuteMap);
        Integer popularMinute = getMapMax(minuteMap);
        guardMaxMinuteMap.put(guardId, guardMaxMinuteMap.get(guardId) + popularMinute);
        if (maxTimesAsleepPerMinute < minuteMap.get(popularMinute)) {
            maxTimesAsleepPerMinute = minuteMap.get(popularMinute);
            guard = guardId;
        }
    }

    private List<D4Shift> filterShiftList(List<D4Shift> shifts, Integer guardId) {
        LinkedList<D4Shift> toRemove = new LinkedList<>();
        List<D4Shift> filteredShifts = new ArrayList<>(shifts);
        for (D4Shift shift : shifts) {
            if (!shift.getGuardId().equals(guardId)) {
                toRemove.add(shift);
            }
        }
        filteredShifts.removeAll(toRemove);
        return filteredShifts;

    }

    private void populateMinuteMap(List<D4Shift> filteredShifts, Map<Integer, Integer> minuteMap) {
        for (int i = 0; i < 60; i++) {
            minuteMap.put(i, 0);
        }
        filteredShifts.forEach(shift -> {
            for (int sleeps = 0; sleeps < shift.getSleepMinute().size(); sleeps++) {
                for (int minute = shift.getSleepMinute().get(sleeps); minute < shift.getWakeMinute().get(sleeps); minute++) {
                    minuteMap.put(minute, minuteMap.get(minute) + 1);
                }
            }
        });
    }

    private void sumMinutesSleptByEachGuard() {
        guardIds.forEach(guard -> guardSleepTallies.put(guard, 0));
        for (D4Shift shift : shifts) {
            guardSleepTallies.put(shift.getGuardId(), guardSleepTallies.get(shift.getGuardId()) + shift.getSleepDuration());
        }

    }

    private void convertEntriesIntoShifts() {
        D4Shift shift = new D4Shift();

        for (int i = 0; i < processInputFile().size(); i++) {
            D4Entry current = processInputFile().get(i);
            D4Entry next = null;
            if (i + 1 != processInputFile().size()) {
                next = processInputFile().get(i + 1);
            }

            //if new guard, create new shift
            if (!current.getEntry().equals(WAKES) && !current.getEntry().equals(ASLEEP)) {
                shift = new D4Shift();
                shift.setGuardId(Integer.parseInt(current.getEntry()));
                guardIds.add(Integer.parseInt(current.getEntry()));
            }
            //if entry is asleep, add start time to list
            if (current.getEntry().equals(ASLEEP)) {
                shift.getSleepMinute().add(current.getMin());
            }
            //if entry is wakes, add wake time to list
            if (current.getEntry().equals(WAKES)) {
                shift.getWakeMinute().add(current.getMin());
            }
            //if next entry is new guard, add shift to list
            if (next != null && (!next.getEntry().equals(WAKES) && !next.getEntry().equals(ASLEEP))) {
                shifts.add(shift);
            }
            if (processInputFile().size() == i + 1) {
                shifts.add(shift);
            }
        }
    }

    private List<D4Entry> processInputFile() {
        List<D4Entry> inputs = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                inputs.add(new D4Entry(scanner.nextLine()));
            }

        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return inputs;
    }
}
