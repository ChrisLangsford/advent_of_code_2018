package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.*;

public class DayOne extends AocSolution {

    private Set<Integer> foundFrequencies = new HashSet<>();
    private Boolean repeatFound = false;

    public DayOne(String inputFilePath) {
        super(inputFilePath);
        foundFrequencies.add(0);
    }

    @Override
    public String runPartOne() {
        Integer sum = 0;
        sum = sumOverFrequencyList(sum);
        return String.format("Day 1 Part 1 Solution: %d", sum);
    }

    @Override
    public String runPartTwo() {
        Integer sum = 0;
        while (!repeatFound) {
            sum = sumOverFrequencyList(sum);
        }
        return String.format("Day 1 Part 2 Solution: %d", sum);
    }

    private Integer sumOverFrequencyList(Integer sum) {
        List<Integer> inputs = processInputFile();
        for (Integer i : inputs) {
            sum += i;
            if (foundFrequencies.contains(sum)) {
                repeatFound = true;
                break;
            } else {
                foundFrequencies.add(sum);
            }
        }
        return sum;
    }

    private List<Integer> processInputFile() {
        List<Integer> inputs = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(getInputFile());
            while (scanner.hasNextLine()) {
                inputs.add(new Integer(scanner.nextLine()));
            }

        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return inputs;
    }
}
