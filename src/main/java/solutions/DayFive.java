package solutions;

import util.AocSolution;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class DayFive extends AocSolution {
    public DayFive(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        Integer result = reactPolymer(processInputFile());
        return String.format("Day 5 Part 1 Solution: %d", result);
    }

    @Override
    public String runPartTwo() {
        List<Integer> lengths = new ArrayList<>();
        for (char i = 'a'; i < 'z'; i++) {
            String input = processInputFile();
            input = input.replaceAll(Character.toString(i), "");
            input = input.replaceAll(Character.toString(i).toUpperCase(), "");

            lengths.add(reactPolymer(input));
        }
        Collections.sort(lengths);
        Integer result = lengths.get(0);
        return String.format("Day 5 Part 2 Solution: %d", result);
    }

    private Integer reactPolymer(String input) {
        String pattern = "aA|Aa|bB|Bb|cC|Cc|dD|Dd|eE|Ee|fF|Ff|gG|Gg|hH|Hh|iI|Ii|jJ|Jj|kK|Kk|lL|Ll|mM|Mm|nN|Nn|oO|Oo|pP|Pp|qQ|Qq|rR|Rr|sS|Ss|tT|Tt|uU|Uu|vV|Vv|wW|Ww|xX|Xx|yY|Yy|zZ|Zz";
        Integer length = input.length();
        Integer newLength = 0;

        while (!length.equals(newLength)) {
            if (newLength != 0) {
                length = newLength;
            }
            input = input.replaceFirst(pattern, "");
            newLength = input.length();

        }
        LOG.info(String.format("%d", input.length()));
        return input.length();
    }

    private String processInputFile() {
        try {
            Scanner scanner = new Scanner(getInputFile());
            return scanner.nextLine();

        } catch (FileNotFoundException e) {
            LOG.error("File not found");
        }
        return "";
    }
}
