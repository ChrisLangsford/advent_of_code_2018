package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import solutions.DayOne;

import java.io.File;

public abstract class AocSolution {

    protected static final Logger LOG = LoggerFactory.getLogger(AocSolution.class);

    private File inputFile;
    private ClassLoader classLoader = getClass().getClassLoader();

    public AocSolution(String inputFilePath) {
        this.inputFile = new File(classLoader.getResource(inputFilePath).getFile());
    }

    public abstract String runPartOne();

    public abstract String runPartTwo();

    protected File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }
}
