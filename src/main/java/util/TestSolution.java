package util;

public class TestSolution extends AocSolution {
    TestSolution(String inputFilePath) {
        super(inputFilePath);
    }

    @Override
    public String runPartOne() {
        return "RUNNING TEST PART 1";
    }

    @Override
    public String runPartTwo() {
        return "RUNNING TEST PART 2";
    }
}
