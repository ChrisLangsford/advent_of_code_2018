package util;

import java.util.Map;

public class MapMethods {
    public static Integer getMapMax(Map<Integer, Integer> map) {
        Map.Entry<Integer, Integer> maxEntry = null;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        if (maxEntry != null) {
            return maxEntry.getKey();
        }
        throw new RuntimeException("Max entry in sleep map was null ");
    }
}
