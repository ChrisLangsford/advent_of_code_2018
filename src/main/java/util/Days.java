package util;

import com.google.common.collect.ImmutableMap;
import solutions.DayFive;
import solutions.DayFour;
import solutions.DayOne;
import solutions.DaySeven;
import solutions.DaySix;
import solutions.DayThree;
import solutions.DayTwo;

public class Days {
    public static final ImmutableMap<Integer, AocSolution> INT_TO_SOLUTION =
            new ImmutableMap.Builder<Integer, AocSolution>()
                    .put(0, new TestSolution(""))
                    .put(1, new DayOne("file\\day_1.txt"))
                    .put(2, new DayTwo("file\\day_2.txt"))
                    .put(3, new DayThree("file\\day_3.txt"))
                    .put(4, new DayFour("file\\day_4.txt"))
                    .put(5, new DayFive("file\\day_5.txt"))
                    .put(6, new DaySix("file/day_6.txt"))
                    .put(7, new DaySeven("file/day_7.txt"))
                    .build();
}