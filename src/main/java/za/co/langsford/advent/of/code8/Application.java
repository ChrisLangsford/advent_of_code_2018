package za.co.langsford.advent.of.code8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import util.AocSolution;
import util.Days;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOG.info("STARTING APP");
        SpringApplication.run(Application.class, args);
        LOG.info("CLOSING APP");
    }

    @Override
    public void run(String... args) throws Exception {
        Integer day = null;
        Integer part = null;
        try {
            day = new Integer(args[0]);
            part = new Integer(args[1]);
        } catch (Exception e) {
            LOG.error("Could not convert arguments to integer!");
        }
        if (day != null && part != null) {
            runSolution(day, part);
        }
    }

    private void runSolution(Integer day, Integer part) {
        AocSolution solution = Days.INT_TO_SOLUTION.get(day);
        LOG.info(String.format("Running Day %s, part %S", day.toString(), part.toString()));
        if (part == 1) {
            LOG.info(solution.runPartOne());
        } else if (part == 2) {
            LOG.info(solution.runPartTwo());
        } else {
            LOG.error(String.format("No such method for part %s, Please enter part as either a 1 or 2", part));
        }
    }
}
